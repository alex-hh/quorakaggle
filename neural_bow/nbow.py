from __future__ import print_function
import numpy as np
import csv, datetime, time, json, sys, os
from collections import defaultdict
from zipfile import ZipFile
from os.path import expanduser, exists
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.models import Sequential
from keras.layers import Embedding, Dense, Dropout, Reshape, Merge, BatchNormalization, TimeDistributed, Lambda, Convolution1D
from keras.layers.merge import Concatenate
from keras.regularizers import l2
from keras.callbacks import Callback, ModelCheckpoint, EarlyStopping
from keras.utils.data_utils import get_file
from keras import backend as K

# note we're assuming utf-8 encoding
# the keras preprocessing functions are very useful: Tokeniser to replace words with integers,
# and pad_sequences, to limit all sequences to a given length.

# What i need to do differently: I need to be able to specify
# whether I want the script to encode the training dataset or the test dataset,
# as I've split these apart before getting embeddings, whereas the script operates
# the other way round. My way is best for what I want because it will allow
# me to compare performance of different scripts on the same test set.

RNG_SEED = 13371447

class Model:
    def __init__(self, model_weights_file='conv_question_pairs_weights.h5',
                 epochs=15, validation_split=0.1, re_weight=True):
        self.model_weights_file = model_weights_file
        self.epochs = epochs
        self.validation_split = validation_split
        self.re_weight=re_weight

    def create_model(self, model_type='fc'):
        if model_type=='conv':
            self.filter_dim = 100

        # model1.add(Convolution1D(filters = nb_filter, 
        #                  kernel_size = filter_length, 
        #                  padding = 'same'))
        # a big question is how to handle words not seen in training
        Q1 = Sequential()
        Q1.add(Embedding(self.nb_words + 1, self.embedding_dim,
                         weights=[self.word_embedding_matrix],
                         input_length=self.max_sent_length, trainable=False))
        if model_type=='fc':
            Q1.add(TimeDistributed(Dense(self.embedding_dim, activation='relu')))
            Q1.add(Lambda(lambda x: K.max(x, axis=1), output_shape=(self.embedding_dim, )))
        elif model_type=='conv':
            Q1.add(Convolution1D(filters=self.filter_dim, kernel_size=3, padding='same'))
            Q1.add(Lambda(lambda x: K.max(x, axis=1), output_shape=(self.filter_dim, )))
        Q2 = Sequential()
        Q2.add(Embedding(self.nb_words + 1, self.embedding_dim, weights=[self.word_embedding_matrix],
                         input_length=self.max_sent_length, trainable=False))
        if model_type=='fc':
            Q2.add(TimeDistributed(Dense(self.embedding_dim, activation='relu')))
            Q2.add(Lambda(lambda x: K.max(x, axis=1), output_shape=(self.filter_dim, )))
        elif model_type=='conv':
            Q2.add(Convolution1D(filters=self.filter_dim, kernel_size=3, padding='same'))
            Q2.add(Lambda(lambda x: K.max(x, axis=1), output_shape=(self.filter_dim, )))
        model = Sequential()
        # model.add(Concatenate([Q1, Q2])) - problem with this was:
        # The first layer in a Sequential model must get an `input_shape` or `batch_input_shape` argument.
        model.add(Merge([Q1, Q2], mode='concat'))
        model.add(BatchNormalization())
        model.add(Dense(200, activation='relu'))
        model.add(BatchNormalization())
        model.add(Dense(200, activation='relu'))
        model.add(BatchNormalization())
        model.add(Dense(200, activation='relu'))
        model.add(BatchNormalization())
        model.add(Dense(200, activation='relu'))
        model.add(BatchNormalization())
        model.add(Dense(1, activation='sigmoid'))

        model.compile(loss='binary_crossentropy', 
                      optimizer='nadam', 
                      metrics=['accuracy'])
        self.model = model

    def load_data(self, which_set='train'):
        print(which_set)
        assert which_set in ['train', 'val', 'test']
        q1_data = np.load(open('q1_{}.npy'.format(which_set), 'rb'))
        print(q1_data.shape)
        q2_data = np.load(open('q2_{}.npy'.format(which_set), 'rb'))
        print(q2_data.shape)
        self.max_sent_length = q1_data.shape[1]
        if which_set != 'test':
            self.labels = np.load(open('label_{}.npy'.format(which_set), 'rb'))
        self.word_embedding_matrix = np.load(open('{}_embedding_matrix.npy'.format(which_set), 'rb'))
        self.embedding_dim = self.word_embedding_matrix.shape[1]
        with open('{}_nb_words.json'.format(which_set), 'r') as f:
            self.nb_words = json.load(f)['nb_words']
            print(self.nb_words)
        setattr(self, 'X_{}'.format(which_set), np.stack((q1_data, q2_data), axis=1))

    def predict(self):
        # ok - big problem: i can't just use the trained weights bc 
        # they involve some specific input dimension size (Q1 Embedding layer)
        # how to make this flexible??
        self.model.load_weights(self.model_weights_file)
        print(self.X_test[:,0].shape, self.X_test[:,1].shape)
        self.preds = self.model.predict([self.X_test[:,0], self.X_test[:,1]]).reshape(-1)

    def save_preds(self):
        with open('preds.csv', 'w') as f:
            fieldnames = ['test_id','is_duplicate']
            w = csv.DictWriter(f, fieldnames=fieldnames)
            w.writeheader()
            for ix, n in enumerate(self.preds):
                w.writerow({'test_id': ix, 'is_duplicate': "{0:.5f}".format(n)})

    def train_model(self):
        Q1_train = self.X_train[:,0]
        Q2_train = self.X_train[:,1]

        # metrics = ['accuracy', 'precision', 'recall', 'fbeta_score']
        # this was original set of metrics but they don't seem to be available

        early_stopping =EarlyStopping(monitor='val_loss', patience=3)
        callbacks = [early_stopping, ModelCheckpoint(self.model_weights_file, monitor='val_loss',
                                     save_best_only=True)]

        if self.re_weight:
            class_weight = {0: 1.309028344, 1: 0.472001959}
        else:
            class_weight = None

        print("Starting training at", datetime.datetime.now())

        t0 = time.time()
        history = self.model.fit([Q1_train, Q2_train], 
                            self.labels, 
                            epochs=self.epochs, 
                            validation_split=self.validation_split, 
                            verbose=1,
                            class_weight=class_weight, 
                            callbacks=callbacks)
        t1 = time.time()

        print("Training ended at", datetime.datetime.now())

        print("Minutes elapsed: %f" % ((t1 - t0) / 60.))


class DataLoader:
    def __init__(self, data_dir='../data/', max_vocab=200000,
                 embedding_file='../glove.6B/glove.6B.300d.txt', embedding_dim=300,
                 max_sent_length=25):
        self.max_vocab = 200000
        self.embedding_file = embedding_file
        self.embedding_dim = 300
        self.max_sent_length = 25
        self.data_dir = data_dir

    def init_tokenizer(self):
        # do for all three sets at once so that the tokenizer has access to all words of interest
        # this will allow the same model to be used for training and test
        # by using the same embedding space
        all_files = {'train': 'train.csv',
                     'test': 'test.csv'}
        self.q1s = defaultdict(list)
        self.q2s = defaultdict(list)
        self.targets = defaultdict(list)

        for dataset, csv_file in all_files.items():
            with open(self.data_dir + csv_file, encoding='utf-8') as f:
                reader = csv.DictReader(f, delimiter=',')
                for row in reader:
                    self.q1s[dataset].append(row['question1'])
                    self.q2s[dataset].append(row['question2'])
                    if dataset != 'test':
                        self.targets[dataset].append(row['is_duplicate'])
            print(dataset + ' question pairs: %d' % len(self.q1s[dataset]))
        questions = []
        for k in all_files.keys():
            questions += self.q1s[k]
            questions += self.q2s[k]
        print('Question pairs: %d' % (len(questions)/2))
        self.tokenizer = Tokenizer(num_words=self.max_vocab)
        self.tokenizer.fit_on_texts(questions)


    def matrix_from_csv(self, which_set='train'):
        os.makedirs(which_set+'_files', exist_ok=True)
        assert which_set in ['train', 'test']
        question1_word_sequences = self.tokenizer.texts_to_sequences(self.q1s[which_set])
        question2_word_sequences = self.tokenizer.texts_to_sequences(self.q2s[which_set])
        word_index = self.tokenizer.word_index

        print("Words in index: %d" % len(word_index))

        # if not exists(KERAS_DATASETS_DIR + GLOVE_ZIP_FILE):
        #     zipfile = ZipFile(get_file(GLOVE_ZIP_FILE, GLOVE_ZIP_FILE_URL))
        #     zipfile.extract(GLOVE_FILE, path=KERAS_DATASETS_DIR)

        # print("Processing", GLOVE_FILE)

        embeddings_index = {}
        with open(self.embedding_file, encoding='utf-8') as f:
            for line in f:
                values = line.split(' ')
                word = values[0]
                embedding = np.asarray(values[1:], dtype='float32')
                embeddings_index[word] = embedding

        print('Word embeddings: %d' % len(embeddings_index))

        # self.nb_words = min(self.max_vocab, len(word_index))
        # this is a fixed dimension which corresponds to the size of the embedding space
        self.nb_words = self.max_vocab
        # word embedding matrix seems to be being initialized to zeros here, which it shouldn't be
        self.word_embedding_matrix = np.zeros((self.nb_words + 1, self.embedding_dim))
        no_glove_count = 0
        for word, i in word_index.items():
            if i > self.max_vocab:
                continue
            embedding_vector = embeddings_index.get(word)
            if embedding_vector is not None:
                self.word_embedding_matrix[i] = embedding_vector
            else:
                # random init of words differentiated by tokenizer but with no glove embedding
                no_glove_count += 1
                pass
        
        # this should be the same as the no glove count - it is
        # worrying that more than half the embeddings are null
        # experiment with different combinations of a smaller max vocab size/
        # random initializations for in vocab words without pretrained embeddings
        print('Null word embeddings: %d' % np.sum(np.sum(self.word_embedding_matrix, axis=1) == 0))
        print('Null word embeddings: %d' % np.sum(np.sum(self.word_embedding_matrix, axis=1)==0))

        self.q1_data = pad_sequences(question1_word_sequences, maxlen=self.max_sent_length)
        self.q2_data = pad_sequences(question2_word_sequences, maxlen=self.max_sent_length)
        self.labels = np.array(self.targets[which_set], dtype=int)
        print('Shape of question1 data tensor:', self.q1_data.shape)
        print('Shape of question2 data tensor:', self.q2_data.shape)
        print('Shape of label tensor:', self.labels.shape)

        np.save(open('q1_{}.npy'.format(which_set), 'wb'), self.q1_data)
        np.save(open('q2_{}.npy'.format(which_set), 'wb'), self.q2_data)
        if which_set != 'test':
            np.save(open('label_{}.npy'.format(which_set), 'wb'), self.labels)
        np.save(open('{}_embedding_matrix.npy'.format(which_set), 'wb'), self.word_embedding_matrix)
        with open('{}_nb_words.json'.format(which_set), 'w') as f:
            json.dump({'nb_words': self.nb_words}, f)


if __name__ == '__main__':
    # first test if data files exist
    if not exists('train_files'):
        dl = DataLoader()
        dl.init_tokenizer()
        dl.matrix_from_csv('train')
        dl.matrix_from_csv('test')
    which_set = 'train'
    if len(sys.argv) > 1:
        which_set = sys.argv[1]
    m = Model()
    m.load_data(which_set)
    m.create_model()
    if which_set == 'train':
        m.train_model()
    elif which_set == 'test':
        m.predict()

# loss, accuracy, precision, recall, fbeta_score = model.evaluate([Q1_test, Q2_test], y_test)
# print('')
# print('loss      = {0:.4f}'.format(loss))
# print('accuracy  = {0:.4f}'.format(accuracy))
# print('precision = {0:.4f}'.format(precision))
# print('recall    = {0:.4f}'.format(recall))
# print('F         = {0:.4f}'.format(fbeta_score))

# if exists(Q1_TRAINING_DATA_FILE) and exists(Q2_TRAINING_DATA_FILE) and exists(LABEL_TRAINING_DATA_FILE) and exists(NB_WORDS_DATA_FILE) and exists(WORD_EMBEDDING_MATRIX_FILE):