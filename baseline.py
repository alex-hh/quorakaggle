import numpy as np
import pandas as pd
import re
from sklearn.linear_model import LogisticRegression

def word2vec(word):
	pass

def loadGloveModel(gloveFile):
    print "Loading Glove Model"
    f = open(gloveFile,'r')
    model = {}
    for line in f:
        splitLine = line.split()
        word = splitLine[0]
        embedding = np.array([float(val) for val in splitLine[1:]])
        model[word] = embedding
    print "Done.",len(model)," words loaded!"
    return model

def train_test_split(datafile):
	df = pd.read_csv(datafile, index_col=0)
	val = df.sample(frac=0.2)
	val_ids = list(val.index)
	train = df.loc[~df.index.isin(val_ids)]
	val.to_csv('data/myval.csv', index_label='id')
	train.to_csv('data/mytrain.csv', index_label='id')

class wordVectorAgg:

	def __init__(self, gloveFile):
		self.wordvecs = loadGloveModel(gloveFile)
		self.word_dim = int(gloveFile[9:].split('.')[2][:-1])
		print(self.word_dim)
		print('loading train data')
		self.load_array('train')
		print('loading val data')
		self.load_array('val')

	def sent2vec(self, sent):
		words = [w.lower() for w in re.findall(r"\w+|[^\w\s]", sent, re.UNICODE)]
		vec = np.mean(np.array([self.wordvecs[w] for w in words if w in self.wordvecs]), axis=0)
		return vec

	def sentSim(self, s1, s2):
		s1 = self.sent2vec(s1)
		s2 = self.sent2vec(s2)
		return self.cosineSim(s1, s2)

	@staticmethod
	def cosineSim(v1, v2):
		if type(v1) == type(v2):
			num = np.dot(v1, v2)
			# print(num, v1, v2)
			if num == 0:
				return num
			return np.dot(v1,v2) / np.sqrt(v1.dot(v1) * v2.dot(v2))
		return np.nan

# unicode issue questions: 190570, 144343

	def load_array(self, which_set='train'):
		# create sentence vectors by averaging wordvectors
		# i could try creating a vector by maxing the wordvectors as well
		assert which_set in ['train', 'val']
		df = pd.read_csv('data/my{}.csv'.format(which_set))
		df = df[(~df['question2'].isnull()) & (~df['question1'].isnull())]
		df.reset_index(inplace=True)
		setattr(self, which_set+'_df', df)
		# the above is crucial as the iteration occurs over the index..
		arr = np.ndarray((len(df), 2, self.word_dim))
		print(arr.shape)
		targets = []
		for i, row in df.iterrows():
			s1v = self.sent2vec(row['question1'])
			s2v = self.sent2vec(row['question2'])
			if np.isnan(s1v).any():
				print(row['question1'], row.id)
				s1v = np.zeros(self.word_dim)
			if np.isnan(s2v).any():
				print(row['question2'], row.id)
				s2v = np.zeros(self.word_dim)
			arr[i,0,:] = s1v
			arr[i,1,:] = s2v
			targets.append(row['is_duplicate'])
		setattr(self, which_set+'_data', arr)
		setattr(self, which_set+'_targets', np.array(targets))

	def compute_sim(self, which_set='train'):
		
		
		df['sim'] = df.apply(self.rowSim, axis=1)
		df['sv1'] = df.apply(lambda row: self.sent2vec(row['question1']), axis=1)
		df['sv2'] = df.apply(lambda row: self.sent2vec(row['question2']), axis=1)
		df['sim'] = df.apply(lambda row: self.cosineSim(row['sv1'], row['sv2']), axis=1)
		setattr(self, which_set + '_df', df)

	def logreg_score(self):
		# logreg classifier on similarity scores
		X_train = np.ndarray((self.train_data.shape[0], 1))
		for ix, pair in enumerate(self.train_data):
			X_train[ix] = self.cosineSim(pair[0],pair[1])
		X_val = np.ndarray((self.val_data.shape[0], 1))
		for ix, pair in enumerate(self.val_data):
			X_val[ix] = self.cosineSim(pair[0], pair[1])
		lr = LogisticRegression()
		lr.fit(X_train, self.train_targets)
		print('Training score', lr.score(X_train, self.train_targets))
		print('Val score', lr.score(X_val, self.val_targets))

	# logistic regression on concatenated sentence vectors gets around 68.1%
	# just guessing 1 all of the time would get 64%...
	def logreg_concat(self):
		# permute_question_labels = self.train_data[:,::-1,:].reshape((-1,self.word_dim*2))
		X = self.train_data.reshape((-1, self.word_dim*2))
		print(X.shape, self.train_data.shape)
		# X2 = np.concatenate((permute_question_labels, X))
		lr = LogisticRegression()
		lr.fit(X, self.train_targets)
		print('Training score', lr.score(X, self.train_targets))
		print('Val score', lr.score(self.val_data.reshape((-1, self.word_dim*2)), self.val_targets))
		
		# logreg classifier on concatenated sentence vectors.

	# I need to split on punctuation
